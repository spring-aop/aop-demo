package org.aop.aopDemo.controller;

import org.aop.aopDemo.dao.AuditDao;
import org.aop.aopDemo.model.AuditLog;
import org.aop.aopDemo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/audit")
@RestController
public class AuditController {

    @Autowired
    private AuditDao auditDao;
    @RequestMapping
    public List<AuditLog> getEmployee(){

        return (List<AuditLog>) auditDao.findAll();

    }
}
