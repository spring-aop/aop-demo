package org.aop.aopDemo.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuditLog {
    @Id
    @Column
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(columnDefinition = "date default sysdate")
    private Date createDate;

    @Column
    private String description;

}
