package org.aop.aopDemo.dao;

import org.aop.aopDemo.model.AuditLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditDao extends CrudRepository<AuditLog, Integer> {
}
