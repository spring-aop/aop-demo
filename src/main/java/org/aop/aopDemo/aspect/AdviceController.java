package org.aop.aopDemo.aspect;

import org.aop.aopDemo.exception.NotFoundException;
import org.aop.aopDemo.model.ResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Optional;

/**
 * The type Advice controller.
 */
@ControllerAdvice
public class AdviceController {

    /**
     * Handle not found response entity.
     *
     * @param notFoundException the not found exception
     * @return the response entity
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ResponseDto> handleNotFound(NotFoundException notFoundException){
        ResponseEntity responseEntity = ResponseEntity.of(Optional.of(ResponseDto.builder().errorMessage("Employee id not found").codes("404").build()));
        return responseEntity;
    }
}
