package org.aop.aopDemo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@ToString
public class Employee {

    @Id
    @Column
    private Integer id;

    @Column
    private String name;

}
