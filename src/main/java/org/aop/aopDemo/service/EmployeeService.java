package org.aop.aopDemo.service;

import lombok.extern.slf4j.Slf4j;
import org.aop.aopDemo.dao.EmployeeDao;
import org.aop.aopDemo.exception.NotFoundException;
import org.aop.aopDemo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    public void addEmployee(Employee employee){
        employeeDao.save(employee);
    }

    public void updateEmployee(Employee employee)throws NotFoundException{
        Employee emp = getEmployee(employee.getId());
        emp.setName(employee.getName());
        employeeDao.save(emp);
    }

    public Employee getEmployee(Integer id){
        return employeeDao.findById(id).orElseThrow(NotFoundException::new);
    }
}
