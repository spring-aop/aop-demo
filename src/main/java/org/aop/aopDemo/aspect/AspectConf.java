package org.aop.aopDemo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aop.aopDemo.dao.AuditDao;
import org.aop.aopDemo.exception.NotFoundException;
import org.aop.aopDemo.model.AuditLog;
import org.aop.aopDemo.model.Employee;
import org.aop.aopDemo.service.EmployeeService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * The type Aspect conf.
 */
@Slf4j
@Aspect
@Configuration
public class AspectConf {

    @Autowired
    private AuditDao auditDao;

    @Autowired
    private EmployeeService employeeService;

    /**
     * Log before all methods.
     *
     * @param joinPoint the join point
     *
     *    Used to log content before all methods
     */
    @Before("execution(* org.aop.aopDemo.service.*.*(..))")
    public void logBeforeAllMethods(JoinPoint joinPoint){
        log.info(joinPoint.getSignature().getName()+ "  Started");
    }

    /**
     * Log after all methods.
     *
     * @param joinPoint the join point
     *                  Used to log content after all methods
     */
    @After("execution(* org.aop.aopDemo.service.*.*(..))")
    public void logAfterAllMethods(JoinPoint joinPoint){
        log.info(joinPoint.getSignature().getName() + "  Ended");
    }


    /**
     * Log before add.
     *
     * @param joinPoint the join point    This method will be executed after the actual method has completed it execution    And returns back to the calling method.
     */
    @AfterReturning("execution(* org.aop.aopDemo.service.EmployeeService.addEmployee(..))")
    public void logBeforeAdd(JoinPoint joinPoint){
        log.info("In advice add");
        String description = "added new Employee::"+joinPoint.getArgs()[0];
        log.info("Add employee advice:: "+ description);
        auditDao.save(AuditLog.builder().createDate(new Date()).description(description).build());
    }

    /**
     * Log before add.
     *
     * @param joinPoint the join point     This method will be executed before the actual method has Starting it execution     And returns back to the calling method.
     */
    @Before("execution(* org.aop.aopDemo.service.EmployeeService.updateEmployee(..))")
    public void logBeforeUpdate(JoinPoint joinPoint){
        log.info("In advice update");

        Employee employee = (Employee) joinPoint.getArgs()[0];
        try {
            Employee oldEmployeeData = employeeService.getEmployee(employee.getId());
            log.info("Updated employee data from  {} to {}", oldEmployeeData, employee);
            String description = String.format("Updated employee data from %s to %s", oldEmployeeData, employee);
            log.info("update employee advice:: "+ description);
            auditDao.save(AuditLog.builder().createDate(new Date()).description(description).build());
        }catch(NotFoundException exception){
            log.error("error occurred: "+exception);
        }
    }

    /**
     * Log error update.
     *
     * @param joinPoint         the join point
     * @param notFoundException the not found exception
     *
     *       Can be used for transaction handling purpose.
     */
    @AfterThrowing(value = "execution(* org.aop.aopDemo.service.EmployeeService.updateEmployee(..))", throwing = "notFoundException")
    public void logErrorUpdate(JoinPoint joinPoint, NotFoundException notFoundException){
        log.info("Error while updating Employee");
        //Rollback logic
        Employee employee = (Employee) joinPoint.getArgs()[0];
        String description = String.format("Error Updating employee details for empid :: %s , Error :: %s",employee.getId(), notFoundException.getMessage());
        log.info("Add employee advice on Error Throw:: "+ description);
        auditDao.save(AuditLog.builder().createDate(new Date()).description(description).build());
    }


}
