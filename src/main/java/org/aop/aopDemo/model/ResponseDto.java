package org.aop.aopDemo.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseDto {
    private String errorMessage;
    private String codes;
}
