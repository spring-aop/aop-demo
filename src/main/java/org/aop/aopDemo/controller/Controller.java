package org.aop.aopDemo.controller;

import org.aop.aopDemo.service.EmployeeService;
import org.aop.aopDemo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/employee")
@RestController
public class Controller {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public void addEmployee(@PathVariable int id, @RequestBody Employee employee){
        employee.setId(id);
        employeeService.addEmployee(employee);
    }

    @RequestMapping(value = "/{id}")
    public Employee getEmployee(@PathVariable int id){
        System.out.println(id);
        return employeeService.getEmployee(id);

    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PATCH)
    public void updateEmployee(@PathVariable int id, @RequestBody Employee employee){
        System.out.println(id);
        employee.setId(id);
        employeeService.updateEmployee(employee);
    }

}
